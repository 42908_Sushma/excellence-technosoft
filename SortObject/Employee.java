public class Employee implements Comparable< Employee >{
  
    private Integer id;
    private String Name;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
     public String getName() {
        return id;
    }
    public void setName(String id) {
        this.id = id;
    }
 
    @Override
    public String toString() {
        return "Employee [id=" + id +"Name"+name+ "]";
    }
 
    @Override
    public int compareTo(Employee o) {
        return this.getId().compareTo(o.getId());
    }
}