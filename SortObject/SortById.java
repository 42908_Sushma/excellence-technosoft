import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
 
public class JavaSort 
{
    public static void main(String[] args) 
    {
        ArrayList<Object> o = getUnsortedObjectList();
         
        //1. Employee ids in ascending order
        Collections.sort(o);
         
        System.out.println(o);
         
        //2. Employee ids in reverse order
        Collections.sort(o, Collections.reverseOrder());
         
        System.out.println(o);
    }
 
    //Returns an unordered list of employees
    private static ArrayList<Object> getUnsortedObjectList() 
    {
        ArrayList<Object> list = new ArrayList<>();
        Random rand = new Random(10);
         
        for(int i = 0; i < 5; i++) 
        {
            Object e = new Object();
            e.setId(rand.nextInt(100));
            list.add(e);
        }
        return list;
    }
}