const arr = [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1];
const consecutiveOnes = (arr = []) => {
   let l = 0;
   let r = 0;
   let max = 0;
   while (r < arr.length) {
      if (arr[r] === 0) {
         if (r - l > max) {
            max = r - l
         };
         r++;
         l = r;
      } else {
         r++
      };
   };
   return r - l > max ? r - l : max;
}
console.log(consecutiveOnes(arr));